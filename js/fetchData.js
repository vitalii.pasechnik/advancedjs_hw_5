import { createLoader } from "./createLoader.js";

const mainLoader = createLoader();
document.body.append(mainLoader);

export async function fetchData() {
    try {
        const dataUsers = await fetch('https://ajax.test-danit.com/api/json/users', { method: 'GET' });
        const dataPosts = await fetch('https://ajax.test-danit.com/api/json/posts', { method: 'GET' });
        const users = await dataUsers.json();
        const posts = await dataPosts.json();
        mainLoader.remove();
        return { users, posts }

    } catch (err) {
        console.log(err);
    }
}



