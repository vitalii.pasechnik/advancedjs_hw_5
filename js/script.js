import { fetchData } from "./fetchData.js";
import { createList } from "./createList.js";

const container = document.querySelector('.container');

async function render() {
    const { users, posts } = await fetchData();
    posts.sort(() => Math.random() - 0.5);

    const userInfo = await users?.map(user => {
        const { id, name, email } = user;
        return { id, name, email }
    })
    createList(posts, userInfo, container);
}

render();

