import { createAddBtn } from "./createBtn.js";
import { showPostModal } from "./createModal.js";
import { addPostToHtml } from "./addPostToHtml.js";

export const createList = (dataPosts, dataUsers, parent = document.body) => {
    const list = document.createElement('ul');
    list.classList.add('list');

    dataPosts.forEach(post => addPostToHtml(list, post, dataUsers));

    createAddBtn(list, showPostModal);
    parent.append(list);
}

