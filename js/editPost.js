import { addPostContent } from "./addPostToHtml.js";

export function editPost(post, inputTitle, inputText, showHint, modalWrapper) {

    const postID = post.getAttribute('id');

    // т.к. посты не сохраняются на сервере, то и изменить добавленный вручную пост нельзя, для этого я исскуственно меняю id котоый больше 100.
    const id = postID > 100 ? 1 : postID;

    if (!!inputTitle.value.trim() && !!inputText.value.trim()) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
            method: 'PUT',
            body: JSON.stringify({
                id: id,
                body: inputText.value,
                title: inputTitle.value
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            if (res.status === 200 && !!res.ok) {
                console.warn(`Status: ${res.status}, Пост ${postID} успешно изменен`);
                addPostContent(post, inputTitle.value, inputText.value);
                modalWrapper.remove();
            } else {
                console.warn(`Error: ${res.status}, Пост ${postID} не удалось изменить`);
            }
        })
    } else {
        showHint();
    }
}