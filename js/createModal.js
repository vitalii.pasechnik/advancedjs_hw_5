import { createNewPost } from "./createNewPost.js";
import { editPost } from "./editPost.js";

export const showPostModal = (post = null) => {

    const postWrapper = document.body.querySelector('.list');
    const modalWrapper = document.createElement('div');
    modalWrapper.classList.add('modal__layer')
    document.body.prepend(modalWrapper);

    modalWrapper.insertAdjacentHTML('afterbegin', `
        <form class="form" action="#" type="POST">
            <span class='esc'>press "Esc" to exit</span>
            <label class="input__wrapper">
                <input class="input input__title" type="text" placeholder="Add Post Title" />
            </label>
            <label class="input__wrapper">
                <textarea class="input input__text" type="text" placeholder="Add Text"></textarea>
            </label>
            <button type="submit" class="btn__submit">Create Post</button>
        </form>
    `)

    const hint = document.createElement('span');
    hint.classList.add("hint");
    const form = modalWrapper.querySelector('.form')
    form.addEventListener('input', hideHint);
    const inputTitle = modalWrapper.querySelector('.input__title')
    const inputText = modalWrapper.querySelector('.input__text')
    const btn = modalWrapper.querySelector('.btn__submit');

    window.addEventListener('keydown', (e) => e.key === "Escape" && modalWrapper.remove());

    if (!!post) {
        const currentTitle = post.querySelector('.post__title').textContent;
        const currentBody = post.querySelector('.post__body').textContent;

        inputTitle.value = currentTitle;
        inputText.value = currentBody;

        btn.textContent = 'Save Changes';
        btn.addEventListener('click', () => editPost(post, inputTitle, inputText, showHint, modalWrapper));
    } else {
        btn.innerHTML = 'Create Post';
        btn.addEventListener('click', () => createNewPost(1, inputTitle, inputText, postWrapper, showHint, modalWrapper))
    }

    function hideHint(e) {
        if (!!e.target.value) hint.textContent = "";
    }

    function showHint() {
        hint.textContent = "* Add all fields";
        inputText.insertAdjacentElement("afterend", hint);
    }
}
