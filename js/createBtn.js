export const createDellBtn = (btnParent, className) => {
    btnParent.insertAdjacentHTML('beforeend', `
        <button class='btn ${className}'>
            <svg class="trash">
                <use xlink:href="./img/icons.svg#trash"></use>
            </svg>
        </button>
    `);
}

export const createAddBtn = (btnParent, createModal) => {
    const btn = document.createElement('button');
    btn.className = 'btn add__btn';
    btn.textContent = 'New Post'
    btnParent.append(btn);

    btn.addEventListener('click', () => createModal());
}

export const createEditBtn = (btnParent, className) => {
    const btn = document.createElement('button');
    btn.className = `btn ${className}`;
    btn.textContent = 'Edit'
    btnParent.append(btn);
}