import { createDellBtn, createEditBtn } from "./createBtn.js";
import { dellPost } from "./deletePost.js";
import { showPostModal } from "./createModal.js";

export const addPostToHtml = (postWrapper, postData, userData = [{ name: 'Bruce Willis', email: 'SuperBruce@gmail.com' }]) => {
    const post = document.createElement('li');
    post.setAttribute('id', `${postData.id}`)
    post.className = "post";
    addPostContent(post, postData.title, postData.body);
    userData.forEach(user => {
        if (user.id === postData.userId || !user.id) addPostAuthor(post, user.name, user.email)
    })

    postWrapper.prepend(post);
    createDellBtn(post, 'dell__btn');
    createEditBtn(post, 'edit__btn');
    const dellBtn = post.querySelector('.dell__btn')
    const editBtn = post.querySelector('.edit__btn')
    dellPost(dellBtn, postData.id, editBtn);
    editBtn.addEventListener('click', () => showPostModal(post))
}

export function addPostContent(post, title, body) {
    const content = post.querySelector('.post__data');
    content?.remove();
    post.insertAdjacentHTML('beforeend', `
        <div class='post__data'>
            <h3 class='post__title'>${title}</h3>
            <p class='post__body'>${body}</p>
        </div>
    `)
}

function addPostAuthor(post, name, email) {
    post.insertAdjacentHTML('afterbegin', `
        <div class='user__info'>
            <span class='user__name'>Author: <strong>${name}</strong></span>
            <span class='email'><a href="mailto:${email}">${email}</a></span>
        </div>
    `)
}