import { createLoader } from "./createLoader.js";

export const dellPost = (btn, postID, editBtn) => {

    const id = postID > 100 ? 1 : postID;
    // т.к. посты не сохраняются на сервере, то и изменить добавленный вручную пост нельзя, для этого я исскуственно меняю id котоый больше 100.

    let delFlag = false;
    let del;
    const loader = createLoader();
    btn.addEventListener('click', () => {
        if (!delFlag) {
            editBtn.setAttribute('disabled', true);
            btn.innerHTML = 'Cancel';
            delFlag = !delFlag;
            btn.parentNode.prepend(loader);
            del = setTimeout(() => {
                fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, { method: 'DELETE', })
                    .then(res => {
                        if (res.status === 200 && !!res.ok) {
                            btn.parentNode.remove();
                            console.warn(`Status: ${res.status}, Пост ${postID} успешно удален`);
                        } else {
                            console.warn(`Error: ${res.status}, Пост ${postID} не удалось удалить`);
                        }
                    })
            }, 4200);
        } else {
            editBtn.removeAttribute('disabled');
            delFlag = !delFlag;
            clearTimeout(del);
            btn.innerHTML = `
                <svg class="trash">
                    <use xlink:href="./img/icons.svg#trash"></use>
                </svg>`
            loader.remove();
        }
    });
}