import { addPostToHtml } from "./addPostToHtml.js";

export const createNewPost = (userID, postTitle, postBody, postWrapper, showHint, modalWrapper) => {
    if (!!postTitle.value.trim() && !!postBody.value.trim()) {
        fetch(`https://ajax.test-danit.com/api/json/posts`, {
            method: 'POST',
            body: JSON.stringify({
                title: postTitle.value,
                body: postBody.value,
                userId: userID,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
            .then(res => {
                if (res.userId === userID) {
                    console.warn(`Пост ${res.id} успешно создан`);
                    addPostToHtml(postWrapper, { userId: userID, id: res.id, title: postTitle.value, body: postBody.value });
                    modalWrapper.remove();
                } else {
                    console.warn(`Error: Пост не удалось добавить`);
                }
            })
    } else {
        showHint();
    }
};